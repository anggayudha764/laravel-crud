<?php

use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Illuminate\Support\Str;
class Benih extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        for ($i=0; $i < 50; $i++) {
              
            \App\Data::create([
                'uuid' => Uuid::uuid4()->getHex(),
            'nama' => 'ini nama',
            'pekerjaan' => str::random(100),
            'tanggallahir'=>date('Y-m-d H:i:s'),
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s'),
        ]);
       }
    }
}
