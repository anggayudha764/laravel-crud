-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 24 Sep 2021 pada 04.49
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ipaydata`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data`
--

CREATE TABLE `data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pekerjaan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggallahir` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data`
--

INSERT INTO `data` (`id`, `uuid`, `nama`, `pekerjaan`, `tanggallahir`, `created_at`, `updated_at`) VALUES
(51, '109b1a03431b47529e86e8d1eaa02792', 'ini nama', 'bgoldkZKPITiG7pNJvjn0Y6wdul0fh8BQqtrf3sF2gc8EXnS3qu0y9ucbpKckrDerbecsclBw1u59AA8eHEKAjFbKj01N3VWzEqi', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(52, 'ca1143680cf44579be2b5eecd13ec72b', 'ini nama', 'Mywe0bf0kU4D2s2QX2rqV1Ko366ddEf1J4cDdJ1qXKPKs51f5SkfQp69Cnn6dC8FeEr7kVHlWCgA834Azjuzc9MmLJS1UaZQkP5b', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(53, 'ac47ac9970ac4b4b9f18f9c02e887891', 'ini nama', 'kvmQunQ5UivbkCa56P12FiamThChGYebJUa3cBPGt28MhpFlyofbZFGhfP6MobaykhDyNqudyGDTbmDlWYnApJw4Sx01CV1FLCSw', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(54, '2a4afd3968434517af72c7be49b66800', 'ini nama', '0Q0PpI4VMTcwQpBdkzdIZGaaC1tTJ6yuhk3qFZhGjkWiV6GJH9bfndW4NucuThrAdYdcZI4BYLYz4SmvA5nxKctdUu5Ly4jSZEuS', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(55, '0c24a5cdd20344f29f41a9cf30c81157', 'ini nama', 'QPv9F4nMcZMLZJdBc4LGj6IgImhD7vB4z1i6u7skekN8I3lGsdrMTaDjgycTp9KBw1FumIIarARR7Eb1uaYiJBEBOqrHNZ7Rwylv', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(56, '3279a5b31b4c465eb42b754c36ab38e1', 'ini nama', 'nB4XG6ncHMmYWfaq5tF57NLCBAVUVPF6oRx7WGkAycFQQJ92v8K3AUHP09dp4qWTvJnycDspQ7L3Z5GUU26HNIyRS7MuBW0Ga5HH', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(57, 'd1100084e0ba4641b04686345385767e', 'ini nama', 'DhwM7wmEI7sOUvIcVFCeGtFW0R0NduUQwIdAiapWPZChRLmhpOmnCQX2xBduh6SJq5BHXl6qCHbL661D7fKMRNdR64uT2MYES5P7', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(58, '4dd48570ffc3405db9df15caa172cb2e', 'ini nama', 'jYNDDhdyYX3EJVLsOrJA3X2o4EO7ugGauDIw3LLjP280QBrin3xzdEh6DK3v9ApdKPoznO7GUWiR62guAfmviJ8U8Kbi9GPcDZK4', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(59, '6f309db9ea3e4c37990551eb3df0917f', 'ini nama', 'gqf5axBDcitLSoUgTNh7LyuPYfKMb58tMMWKw8nT2iPvg4AB7B5QNONnSDLu9wjx2AjZvfTQxVHV5lm4uOTtMgJ4EYeZqHCvoAds', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(60, 'a03a2dccb75b45de82c9cce2695dcd07', 'ini nama', 'HKJ6xuWuJIxVteec00Yvr1LRcxTUguxD9dNn2klPLr96KdBpQ14I3UFp7vkYofhgu5d2kUdaNdlMnuRJjHTFs2vj7n0G4d9sLjvN', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(61, '5eb049ca95604083aaaea749cbb940dd', 'ini nama', 'mPJq7FQAY1E6rJl3x8s5dNyKBzdpQMwGWAuiRC0RXYz95MUJvlVBco0gOh52w5U7dKw9ZP7qpogTSvaw0oHw5xXIIfXA7b9sT57q', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(62, 'ca14b624e7bc4458a2d104bd7f5762a2', 'ini nama', 'sdYJcjqPllnxxz2oHcTnysj0RawQUznF4PvbPiJ94TPaJa6egaI5107sUXlYkEue66n2zxuEEREaDJPBsLzWKeOEsRdMgzY9xv8E', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(63, '0e8c7252bf634375aefd36e376d605c0', 'ini nama', 'FYc0At2RPLknIyXt5KcN17QRGdXBPAfEU78HvCYsZfMszRZMBIfaWCpgf98IfNQ1UrzQrf737ViaBYk1jPomIkFiZMvhHJLNAE77', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(64, 'be211642741b44e596903ad9c4c35df0', 'ini nama', 'v5mmn03ZoaRCtg8tXmc9pJmX4fMQB0xUkCKLDGUEYUv1G7Xl4Yys8w9hfxo7MmQxu8rdDEQwMClx4WFuxRPHymfjU6EFd2dXsXLb', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(65, 'cbed0d18696945f7a115809226059e58', 'ini nama', 'wBNYvwmn36VSlWp7GlAalEXBhNeJjdX5pOzW6VUGP9mAnXndsd4n5WEEFqdXjc7oLB2d2dDySmqyQRHq9Vg6xDSy6iGh9shJrxb3', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(66, 'd8819856402d4dd19674358663e587e5', 'ini nama', 'WNo8z1jnbxNJEjUJCkWNIFc45gAhvTVHO3UBqTEhMrWcBk0Yq1VAOB0r5PWycGuYfNqiqmDBjxhjN6aWBtckshiIxpDd79n8Sw9N', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(67, 'f71af0bf5fff4f43a9e78e4497910f81', 'ini nama', 'u3EHTbOp0nPaiHHRCALAFdVCrGGTbdJoEPpaclSfiKU7r2XgGKV4JliY2zxMTDSOYQh0RJ2rI8so8aHtVwHshoIXOvvfMAC9mwv8', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(68, '6a6f6c09aeb94608974907b0fca7a0f0', 'ini nama', 'h88kUByIZywCjGiz9Zml9gc89EUIvanU5Zzo3ygIJZojQRuctn2s2l2ZCESwcwIYtkYELFWQTiabzcQiSAM9BuYBgkWYXGg8oAdO', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(69, '34747ac9bde84eaa80e67c12ceda3a1f', 'ini nama', 'wV1vnAJzqY8DdMXrCCL9DQeLhtg1pCYhmL7o1QC6B35ceVQo1wELvuOor4FOkeqidqoHNz79wcOxrt3cMbs1LXQPOfaGBzrSwWyd', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(70, '8fc5f2a8259142cb8d79a3ec22213072', 'ini nama', 'vfGmm8ZwtejyrStSnnY0ob8Q5Ly7hJmmuP5ZoVSCMMzGcykZS53WbKTmsV06iRZLp9kpNsVGwQHQN87c1F2pTt6Kpm43Evda5Tsh', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(71, 'd8444820142947fea640902f7e776e3b', 'ini nama', 'ynV87Y2H0rSTFMwszodMdSiiDWIw29AERVzZftAKD2AdPTgrtDSk0YkiJE6X78FefEmEqlaLWG39zKNRZZIURkfj1GVk4FPnWyxP', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(72, '5cf7130058b14c0ca37224bf4c0d3d9b', 'ini nama', 'TvlDdJkuXrEoxNRFOg3oY0vNkZw6SZRIlgea6Kmr5B1FmJbNOlvF3lrClX63ErUylqNeTSui5JxVdsfAMoxmUG6eJeAdltYCcrRT', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(73, '02749d67add04914a3ea4e7cbb563e92', 'ini nama', 'AozozvnA3BGVWzjNbKHo4VxY2MBiUrS5iVKvVPbJnLSCjAS8Mbgtudxqb5p56zMAd2R0HTHZVGKK6d5g50FYR74P0gPPd7dsyxUu', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(74, '59a7ae8f466e486a836d13da8d0798ad', 'ini nama', 'JyaRtRx6Bw9Ohn67rMdb1nvAof64LXauwplGxsLEiJibyMQFc5mpkc7uv1UmYGUWvW5l4HhyCeDp4HRC2Q3jw8StECEun2aocbTD', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(75, '20fa71c448274a588ba0424af7d89df0', 'ini nama', 'LknGB7PrKaeoRwRObWCkbwvZSJmyzIojpLYi82QSXJmxDP4Uah2gH5izy6gIfgWEHuXwL85YuBJENrnoO1ioWJBgliEelnoNCL66', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(76, '7fac22417943458092dcd1e3b2e3b7f5', 'ini nama', 'uIRUp1LKo5okVTVrwUyYu5Qa7zKb6qQQpTXiEjLidCGRqIBE9wx10l1pV72p8T3iqJys80kF5gAEcPpTC3UBTKIWRNLDF6LhxlHW', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(77, '8d2a7a29ed314d99a6b6ebec8aa2b978', 'ini nama', 'wRpG8NoF0QKgIJY8JjbixXSvY1Yj4aFT3hbrn9aswY951Nf2cC4LU53Kl9tLhaQcFfwRV5O8kqWQLyOc3UXqDhSaZbLETa7XGX2X', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(78, 'd330182eefdf458c9c10c8101b6756dc', 'ini nama', 'R5LZfQDcd4wIJieEC1XwptUCa4A7btid7wMeWj12n2m1sRqVLzLmNDQEangGHaIFYjO1p49v8zSlDzUKNYJOvm13ABVqLF6RD8yy', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(79, '14118707b800496a8e1e4ab297798eb5', 'ini nama', 'dy69WNmWXgnfIFQDYYYZt0r9MjuGH9kwg5Jz8kOQravg5LN7oYSxzlyL0YXvzA1U5YgE6cDldKl8RQesKH411wxYfuNWQFPq9H9z', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(80, '78cf986050cb4275a33f1d3768af7a5e', 'ini nama', '8YLF6yClqVywZBw6mYDmp9tUO1uzMBOP1yphraAELhBUvUWRJ57kwMFt1g8cyBllv8obb2VGefT1zrai3eVenTp7GSG0N97HHVmJ', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(81, '58029da7ef9d4599a725356c1d9d2130', 'ini nama', 'MIjoFZlNI6Z2ZHSV6ETllwJtdmTLSlZksIeoHgs297hZxKdcZJ9WP0vHTWaP7OuED6JBtjd79yfAn0BhXR8ZmytLVEnuVMqeI3H9', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(82, '0b44fb6fa7b948d4bf5c94b841eebafc', 'ini nama', 'szrvucjXZe8uyz2wRI2evzjS8eKQrMyX0dweEgTLXiI4DDNZnt1Osjj29ZhW4YqJaIUXpP3Xor9z0v9Q5wnAu4oXGMgB4wWEm4Gz', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(83, '65b170b042964fe19f1565b848fdf053', 'ini nama', 'qMJvmAjFo65Shmc3av2ttglI28jfqC0hbomEuytuzQvnDFFefiHD08thADjUUhxzq8QkkCWTebqBJNqmfBCpGm8c0TJVZXUMa15p', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(84, 'b6eedbccdd984cd1b133dbc61f03978c', 'ini nama', 'UxtTKy3UWuvFyFWZNU8e3LPOt4iIdt9DdnIBU9drd5OWPjEWKQcctLKWRBUnMKRkyy0EgSsV0R1qU2wqq6yJ4aFAWEZArz4K5OA0', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(85, '944cc718ca5f44ff84ab882cafa4b6dd', 'ini nama', 'YlJc660Wvz7qjS8CZJ1gM6wptN8tvCzN9EmtLqBN4csy8as0pqe1306vamfS5jxohsj1LDS6gfWkXkTEW2C0xKlK2N8EamwAjqLh', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(86, 'c5a77d6e13284a409dea6dc5a5a95e57', 'ini nama', 'TgtWgB6oCGAPA4d2eIYAy6xyEu6vLFPMKbgPgJSpLmw1O7CzHTSp0fW0QVFCK2PSszjHJE0hdVnxFnNnvt8fGQAY9t2sgqoR15qI', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(87, 'bf694e2a174e45f392bac7aba144117e', 'ini nama', 'JTm1JTTCtcR9UUVGWGlGJ6ZpttFrHFItYvI8dCvuI5OxrSlKivfF7AgiZW7M6WbP9OB3bAxLqxTZuzBNRAglFFwM8ZbPCA4KyHmD', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(88, '6e8ce04eb5da4f88a8420c6659164ab1', 'ini nama', 'ZqldFfv2bdz9IuW48HdjispO5xVG9ghDWwHGuC9uYfxoE39d1wddnluIlYQPfnyTLgY8KwG2gSRSLybfIXawEzx2IGhIDfzx8byc', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(89, '53dd6bd287404262aa381d56d24f5e96', 'ini nama', 'braJUE9rsbpkgnoXGnrRDgrlAqAZH2s09O0QzklhFk7lZyHDtShug0dwDx2Zts9DwqbQM7j8b2SM6soEGWU1ai1qcSOr5AsOrjkB', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(90, '359dfae493bf49eba317378146ebc43c', 'ini nama', 'VoXOKVFemHMRpe2UeSJnAtI5I02WAeJ5UoWoiSXEAh0G4e7z8nElPlkMBn4tNM0UhZYZBjqQmK6simAUgH7kaKJr5lenf6MxyU9N', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(91, 'a22adf1fa2a4465ebd611e9585484dce', 'ini nama', 'p7ZvSwjNZWN2UXLWJNa4VrDO9BbIHRPYziGyQi2ioovhWyD4cyEbnteRrqqRNxuTT9EqlK7saipPqxBfQcVy2V6Bx9XAMyFU42e3', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(92, '0024f7d1af4a46dbae31b4712d6045bc', 'ini nama', 'f1w5xO3c3kYAGHQ7vv12fx1cJebdRMppA0M8RwjRHgHWayHa53vxzy4VRoOnDwU4LRqZ9CqF92vc3xTV2F7LG3jLaaZhndXIB9jM', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(93, 'd8b686d318e94d31a5435cbdbb92f7db', 'ini nama', '1Rz9pcjeFlI0Jb5oe6l0d007FWVsrJGytVMbsjXtPOlOH8Nhef7SYqzUEG7Il0gPQk4nod71GJYGXFRONMfamfBEmOeKJmoL1wHe', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(94, '0a2b83e7592e4455a1cf5568686fb251', 'ini nama', '66FpCOMeVMLFkqZefTqbGlVtYSHbhb7nx0Kp11MNHsGKTxhLPiOd199D37LsTczSeRXJS7VbtShpY9ZsSoKL8tk41TuyBYpGA84i', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(95, '9d612974549542e6a29c72a78b4d8d36', 'ini nama', '7vwreKWhNuPFAtr2TCOfwQuR9Uurq6EZKigH8e4syzmsDFKh094klF4ElLvlrjdXvLZA4v7w8mEkETcSckP2XNvPbVqN1GTHTU7B', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(96, 'bede8796404c4291a947dcbdc6078cbf', 'ini nama', 'jnbhjNoUypVbsKjipCjuzSk0sRO5SOlx7yJQJYKiLTr7SaOsVuyw6goUUCdUtq3hm6mKnIR9z6wBytNUfl7ZFfaSlzKfqGMSs1WQ', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(97, '374a0185a45d4e7d9ebc3393c7131750', 'ini nama', 'BuKxzsFwoXaFwQJyWhdEPbFbAXrVhEZ3qjK6g7VfJzBHpZo9rmAbMj1mnNnKUfiBmSDUBKo8VrbPez4CjvU31huPDfU2E8jBIRpu', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(98, 'a67d87224bb4410b92375ee6927f8d95', 'ini nama', 'OLMUzNuJlZUPbV1RqS9MWiVmWTpd1zBuJdznEhGEP3k544bRr1vy9txekMsK8wWMUS4CedFGHo1bVZywzzA2kAmpIEIoEAnDtfR2', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(99, '98fb8ca2ef214c82b30d083af851f374', 'ini nama', 'gy0UyJLJH4x3S0Q4wIGqZM3JszvXRTj798fu93iL5nxjdeM5lmPa90N9g7S49GYxb8qMibBeDzUwbROvSqtKDx8SPttzEjdJQC8G', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24'),
(100, '2d615d9057934726a7302958258dddcf', 'ini nama', 'rOlaU9hMckkK1F1md03zQsUS1vPY3PFzVsBTsIb5jRRQyQmfQASbUTZ3AT7foLEmQxxuf3IZ3khOItEqafz5NxL2saL03J5UPKTe', '2021-09-23', '2021-09-22 22:06:24', '2021-09-22 22:06:24');

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(5, '2014_10_12_000000_create_users_table', 1),
(6, '2014_10_12_100000_create_password_resets_table', 1),
(7, '2019_08_19_000000_create_failed_jobs_table', 1),
(8, '2021_09_23_020048_create_data_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `data`
--
ALTER TABLE `data`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `data`
--
ALTER TABLE `data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
