<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        for ($i=0; $i < 50; $i++) {
              
            \App\User::create([
                'uuid' => Uuid::uuid4()->getHex(),
            'nama' => str::random(12),
            'pekerjaan' => str::random(100),
            'tanggallahir'=>date('Y-m-d H:i:s'),
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s'),
        ]);
       }
    }
}
