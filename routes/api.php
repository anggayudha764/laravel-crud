<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/articles', 'DataController@index');
Route::post('/article/store', 'DataController@store');
Route::get('/article/edit/{id}', 'DataController@getArticle');
Route::get('/article/show/{id}', 'DataController@getArticle');
Route::put('/article/update/{id}', 'DataController@update');
Route::delete('/article/delete/{id}', 'DataController@delete'); 
