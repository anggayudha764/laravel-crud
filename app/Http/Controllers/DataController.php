<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

class DataController extends Controller
{
    public function index()
    {
        $articles = \App\Data::all();
 
        return $articles->toJson();
    }
 
    public function store(Request $request)
    {
        $validatedData = $request->validate([
          'nama' => 'required',
          'pekerjaan' => 'required',
          'tanggallahir' => 'required',
        ]);
 
        $project = \App\Data::create([
          'nama' => $validatedData['nama'],
          'uuid' => Uuid::uuid4()->getHex(),
          'pekerjaan' => $validatedData['pekerjaan'],
          'tanggallahir' => $validatedData['tanggallahir'],
        ]);
 
        $msg = [
            'success' => true,
            'message' => 'Article created successfully!'
        ];
 
        return response()->json($msg);
    }
 
    public function getArticle($id) // for edit and show
    {
        $article = \App\Data::find($id);
 
        return $article->toJson();
    }
 
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
          'nama' => 'required',
          'pekerjaan' => 'required',
          'tanggallahir' => 'required',
        ]);
 
        $article = \App\Data::find($id);
        $article->nama = $validatedData['nama'];
        $article->pekerjaan = $validatedData['pekerjaan'];
        $article->tanggallahir = $validatedData['tanggallahir'];
        $article->save();
 
        $msg = [
            'success' => true,
            'message' => 'Article updated successfully'
        ];
 
        return response()->json($msg);
    }
 
    public function delete($id)
    {
        $article = \App\Data::find($id);
        if(!empty($article)){
            $article->delete();
            $msg = [
                'success' => true,
                'message' => 'Article deleted successfully!'
            ];
            return response()->json($msg);
        } else {
            $msg = [
                'success' => false,
                'message' => 'Article deleted failed!'
            ];
            return response()->json($msg);
        }
    }
}
